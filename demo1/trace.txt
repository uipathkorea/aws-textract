[OCR confidence threshold]
95

[필드 1]

key: SWIFT CODE

value parts:
(OCR confidence / value / new value)
99.1578903198242 / CITIHKHX

value: CITIHKHX

[필드 2]

key: BANK NAME

value parts:
(OCR confidence / value / new value)
97.0244293212891 / Citibank,
92.0594177246094 / N.A., / NA.,
99.9253463745117 / Hong
99.8273010253906 / Kong
99.9505004882813 / Branch

value: Citibank, NA., Hong Kong Branch

[필드 3]

key: ADDRESS

value parts:
(OCR confidence / value / new value)
99.8921966552734 / 26/F
99.9546279907227 / Tower
99.7764434814453 / One
99.9201507568359 / Time
99.7881774902344 / Square
58.9619140625 / 1 / 1
99.9487762451172 / MATHESON
99.784553527832 / STREET
99.9149627685547 / CAUSEWAY
98.2072372436523 / BAY
48.3801383972168 / HK / HK

value: 26/F Tower One Time Square 1 MATHESON STREET CAUSEWAY BAY HK

[필드 4]

key: ACCOUNT NO

value parts:
(OCR confidence / value / new value)
99.7913284301758 / 3355004774664

value: 3355004774664

[필드 5]

key: PAYEE

value parts:
(OCR confidence / value / new value)
99.4380035400391 / JORI
99.8228759765625 / GROUP
93.5889511108398 / CO., / CO,
99.8341979980469 / LIMITED

value: JORI GROUP CO, LIMITED

[필드 6]

key: BANK ADDRESS

value parts:
(OCR confidence / value / new value)
99.8929595947266 / Citibank
94.8870315551758 / Tower, / Tower,
98.4632949829102 / Citibank
99.736686706543 / Plaza,
99.8794708251953 / 3
99.9730834960938 / Garden
99.8644256591797 / Road,
99.8239364624023 / Central,
99.7756805419922 / Hong
99.2470474243164 / Kong

value: Citibank Tower, Citibank Plaza, 3 Garden Road, Central, Hong Kong

